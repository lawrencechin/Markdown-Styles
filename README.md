<h1 class="fresh_heading">Crisp Fresh Air</h1>
<h2>So <i class="fresh_heading2">fresh</i>, so <i class="fresh_heading3">delight</i>!</h2>

![swatch](swatch.png)

A light **css** style based on the editor theme for [Mou](http://25.io/mou/). Originally it was made for use with the app [Macdown](https://macdown.uranusjr.com), latterly in conjunction with [Marked 2](https://marked2app.com) and now used for a website derived from **markdown** files. 

It features a dark-mode invoked by adding `.inverted` to the HTML element.
